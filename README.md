# ryo
A reimplementation of the [STKAddons online service](https://github.com/supertuxkart/stk-addons) in Python using the Quart web framework. Named after Ryo Yamada from Bocchi The Rock.

> [!WARNING]
> Ryo is **highly experimental and incomplete** and can contain bugs. Do not use in production yet.

# Roadmap

- [x] Users
- [x] Registration
    - [x] Account creation
    - [x] Taken username and email check
    - [ ] Email verification (no resources to make this possible yet.)
    - [ ] Prohibit email delimiters (ex. `kitakita+stk@disroot.org`)
- [x] Authentication
    - [x] Login
    - [x] Temporary Sessions
    - [x] Multi-session (no more invalidating existing session)
    - [x] Discard session
- [x] User Lookup
- [ ] Account management
    - [ ] Change password
    - [ ] Change email
- [ ] Relationships (friends system)
- [ ] Ranking
    - [ ] Ranking submission
- [ ] Addons
    - [ ] Voting system
    - [ ] User generated addons
- [ ] Servers
    - [ ] Server authentication
    - [ ] Server lists
- [ ] Polling
    - [ ] Friend notifications
- [ ] Frontend
    - [ ] Homepage
    - [ ] Dashboard
    - [ ] Addon browser
    - [ ] Addon uploading
    - [ ] Site administration
    - [ ] User profile viewing
    - [ ] Player rankings
    - [ ] Account preferences
        - [ ] Delete Account
- [ ] REST API?

# How to run

1. Clone repository

```
git clone https://codeberg.org/kita/ryo
cd ryo
```

2. Create a virtual environment and activate it

```
python -m venv venv
source venv/bin/activate
```

3. Install deps

```
pip install -r requirements.txt
```

(todo)

# Report bugs
If you found a bug, create an issue [here](https://codeberg.org/kita/ryo/issues/new). If you found a security vulnerability please contact me via email (can be found in my profile)
