import aiosqlite
from sqlite3 import PARSE_DECLTYPES

from quart import current_app, g


async def getdb() -> aiosqlite.Connection:
    if 'db' not in g:
        g.db = await aiosqlite.connect(
                current_app.config['DATABASE'],
                detect_types=PARSE_DECLTYPES
                )
        g.db.row_factory = aiosqlite.Row

    return g.db


async def closedb(e=None):
    db = g.pop('db', None)

    if db is not None:
        await db.close()


async def initdb():
    db = await getdb()

    async with await current_app.open_resource('schema.sql') as f:
        await db.executescript((await f.read()).decode('utf-8'))


def initapp(app):
    app.teardown_appcontext(closedb)
