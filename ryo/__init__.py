import os

from quart import Quart

__version__ = "0.1.0"

from . import api


def create_app():
    app = Quart(__name__, instance_relative_config=True)
    app.config.from_mapping(
            DATABASE=os.path.join(
                app.instance_path, 'ryo.sqlite'
                ),
            SECRET_KEY='youshallnotpass'
            )

    app.config.from_pyfile('ryoConfig.py', silent=True)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import db
    db.initapp(app)

    @app.before_serving
    async def before():
        await db.initdb()

    app.register_blueprint(api.blueprint)
    return app
