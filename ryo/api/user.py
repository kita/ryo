from quart import (
        Blueprint,
        request,
        abort,
        Response,
        )
from ..utils import unsuccessfulRequest
from .. import db as _db
from .. import auth

import xml.etree.ElementTree as et
from sqlite3 import IntegrityError
import secrets
from werkzeug.security import generate_password_hash, check_password_hash

bp = Blueprint('user', __name__, url_prefix='/user')


@bp.post('/poll/')
async def poll():
    f = await request.form
    await auth.checkAuth(f)

    return Response(
            et.tostring(
                et.Element(
                    'poll',
                    {
                        'success': 'yes',
                        'info': ''
                        }
                    )
                ),
            mimetype="application/xml"
            )


@bp.post('/connect/')
async def connect():
    f = await request.form

    username = f.get("username", None)
    password = f.get("password", None)
    saveSession = f.get("save-session", 'true')

    if not username:
        return unsuccessfulRequest(
                'connect',
                'Username required'
                )

    if not password:
        return unsuccessfulRequest(
                'connect',
                'Password required'
                )

    db = await _db.getdb()

    user = await (await db.execute(
            "SELECT * FROM users WHERE username = ?", (username,)
            )).fetchone()

    if user is None:
        return unsuccessfulRequest(
                'connect',
                'Wrong username or password',
                401
                )
    elif not check_password_hash(user['password'], password):
        return unsuccessfulRequest(
                'connect',
                'Wrong username or password.',
                401
                )

    token = secrets.token_hex(24)

    if saveSession == 'true':
        await db.execute("""
            INSERT INTO sessions (id, sessionid, useragent)
            VALUES (:id, :sessionid, :useragent)
        """, {
            'id': user["id"],
            'sessionid': token,
            'useragent': request.user_agent.string})
    else:
        await db.execute("""
            INSERT INTO sessions
            (id, sessionid, istemporary, expires, useragent)
            VALUES
            (:id, :sessionid, TRUE,
            datetime('now', '+2 hours', 'localtime'), :useragent)
        """, {
            'id': user['id'],
            'sessionid': token,
            'useragent': request.user_agent.string
            })
    await db.commit()

    return Response(
            et.tostring(
                et.Element(
                    'connect',
                    {
                        'success': 'yes',
                        'token': token,
                        'username': user['username'],
                        'realname': '',
                        'userid': str(user['id']),
                        'achieved': '',  # TODO: Achievements
                        'info': ''
                        }
                    )
                ),
            mimetype="application/xml"
            )


@bp.post('/saved-session/')
async def savedsession():
    f = await request.form
    await auth.checkAuth(f)

    db = await _db.getdb()

    user = await (await db.execute(
        "SELECT * FROM users WHERE id = ?", (f['userid'])
        )).fetchone()

    return Response(
            et.tostring(
                et.Element(
                    'connect',
                    {
                        'success': 'yes',
                        'token': f['token'],
                        'username': user['username'],
                        'realname': '',
                        'userid': str(user['id']),
                        'achieved': '',  # TODO: Achievements
                        'info': ''
                        }
                    )
                ),
            mimetype="application/xml"
            )


@bp.post('/register/')
async def register():
    db = await _db.getdb()
    f = await request.form

    username = f.get('username', None)
    password = f.get('password', None)
    passwordConfirm = f.get('password_confirm', None)
    email = f.get('email', None)
    terms = f.get('terms', None)

    if not username:
        return unsuccessfulRequest('registration', 'Username required')

    if len(username) < 3 or len(username) > 30:
        return unsuccessfulRequest(
                'registration',
                'Username must between 3 and 30 characters long'
                )

    if not password:
        return unsuccessfulRequest(
                'registration',
                'Password required'
                )

    if len(password) < 8 or len(password) > 64:
        return unsuccessfulRequest(
                'registration',
                'Password must be between 8 and 64 characters long'
                )

    if not passwordConfirm or (password != passwordConfirm):
        return unsuccessfulRequest(
                'registration',
                'Passwords do not match'
                )

    if not email:
        return unsuccessfulRequest(
                'registration',
                'Email required'
                )

    if not terms or (terms != 'on'):
        return unsuccessfulRequest(
                'registration',
                'You must agree to the terms to register'
                )

    try:
        await db.execute(
            'INSERT INTO users (username, password, email) VALUES (?, ?, ?)',
            (username, generate_password_hash(password), email)
        )
        await db.commit()
    except IntegrityError as e:

        if str(e) == 'UNIQUE constraint failed: users.username':
            return unsuccessfulRequest(
                    'registration',
                    'This username has been taken. Sorry!'
                    )
        elif str(e) == 'UNIQUE constraint failed: users.email':
            return unsuccessfulRequest(
                    'registration',
                    'This email has been taken. Have you registered before?'
                    )

    return Response(
            et.tostring(
                et.Element(
                    'registration',
                    {'success': 'yes', 'info': ''}
                    )
            ),
            mimetype="application/xml"
            )


@bp.post("/disconnect/")
async def disconnect():
    f = await request.form
    await auth.checkAuth(f)

    db = await _db.getdb()

    await db.execute("DELETE FROM sessions WHERE sessionid = ?",
                     (f['token'],))
    await db.commit()

    return Response(
            et.tostring(
                et.Element(
                    'disconnect',
                    {
                        'success': 'yes',
                        'info': ''
                        }
                    )
                ),
            mimetype="application/xml"
            )


@bp.post("/get-addon-vote/")
async def getAddonVote():
    abort(501)


@bp.post("/set-addon-vote/")
async def setAddonVote():
    abort(501)


@bp.post("/get&-friends-list")
async def getFriendsList():
    abort(501)


@bp.post("/user-search/")
async def userSearch():
    f = await request.form
    await auth.checkAuth(f)

    searchString = f.get("search-string", None)

    if not searchString:
        return unsuccessfulRequest(
                'user-search',
                'Search query required'
                )

    db = await _db.getdb()

    sqlesc = str.maketrans({
        "%": "\\%",
        "_": "\\_",
        "\\": "\\\\"
        })

    users = await (await db.execute(
        "SELECT id, username FROM users WHERE username LIKE ?",
        ("%" + str(searchString).translate(sqlesc) + "%",)
        )).fetchall()
    root = et.Element(
            'user-search',
            {
                "success": "yes",
                "search-string": searchString,
                "info": ""
            }
        )

    u = et.SubElement(root, 'users')

    for x in users:
        et.SubElement(u, 'user', {"id": str(x[0]), "user_name": x[1]})

    return Response(
            et.tostring(root),
            mimetype="application/xml"
            )
