from quart import (
        Blueprint, Response
)

import xml.etree.ElementTree as et

from .user import bp as userbp
from .. import __version__
from ..utils import unsuccessfulRequest
from ..auth import SessionInvalid, NonExistentUser
from werkzeug.exceptions import (
        InternalServerError, NotFound,
        MethodNotAllowed, NotImplemented
        )

blueprint = Blueprint('api', __name__, url_prefix="/api/v2")
blueprint.register_blueprint(userbp)


@blueprint.errorhandler(InternalServerError)
async def internalerrorhandler(e: InternalServerError):
    return unsuccessfulRequest(
            'ryo',
            f'ryo/{__version__}: Internal Server Error: {e.original_exception}',
            500
            )


@blueprint.errorhandler(NotFound)
async def notfound(e):
    return unsuccessfulRequest(
            'ryo',
            f'ryo/{__version__}: Not found',
            404
            )


@blueprint.errorhandler(MethodNotAllowed)
async def bruhwtf(e):
    return unsuccessfulRequest(
            'ryo',
            f"ryo/{__version__}: Method Not Allowed (coming from web browser?)"
            )


@blueprint.errorhandler(NotImplemented)
async def notimplemented(e):
    return unsuccessfulRequest(
            'ryo',
            f'ryo/{__version__}: Not Implemented.',
            501
            )


@blueprint.errorhandler(SessionInvalid)
async def invalidsession(e):
    return unsuccessfulRequest(
            'ryo',
            'Session not valid. Please sign in.',
            401
            )


@blueprint.errorhandler(NonExistentUser)
async def nonexistentuser(e):
    return unsuccessfulRequest(
            'ryo',
            'User does not exist.',
            404
            )


@blueprint.route("/version")
async def version():
    return Response(
            et.tostring(
                et.Element(
                    'ryo',
                    {'version': __version__}
                    )
                ),
            mimetype="application/xml"
            )
