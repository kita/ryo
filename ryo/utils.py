from quart import Response

import xml.etree.ElementTree as et


def unsuccessfulRequest(component: str,
                        info: str, code: int = 400) -> Response:

    return Response(
            et.tostring(
                et.Element(
                    component,
                    {
                        'success': 'no',
                        'info': info
                        }
                    )
                ),
            mimetype="application/xml",
            status=code
            )
