from . import db as _db


class SessionInvalid(Exception):
    pass


class NonExistentUser(Exception):
    pass


async def checkAuth(f):

    if f.get('userid', None) is None or f.get('token', None) is None:
        raise SessionInvalid

    db = await _db.getdb()

    user = await (await db.execute("SELECT * FROM users WHERE id = ?",
                                   (f['userid'],))).fetchone()

    if user is None:
        raise NonExistentUser

    sessions = await (
            await db.execute("SELECT sessionid FROM sessions WHERE id = ?",
                             f['userid'])).fetchall()

    if sessions is None:
        raise SessionInvalid
    else:
        for x in sessions:
            if x[0] == f['token']:
                return
        raise SessionInvalid
